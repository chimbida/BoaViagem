package com.senac.chimbida.boaviagem;

import android.app.Activity;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;

/**
 * Created by chimbida on 4/24/2017.
 */

public class ConfiguracoesActivity extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.configuracoes);
        addPreferencesFromResource(R.xml.preferencias);
    }
}
