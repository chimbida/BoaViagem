package com.senac.chimbida.boaviagem;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by chimbida on 4/24/2017.
 */

public class BoaViagem extends Activity {


    private static final String MANTER_CONECTADO="manter_conectado";

    private EditText usuario;
    private EditText senha;
    private CheckBox manterConectado;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        usuario = (EditText)findViewById(R.id.usuario);
        senha = (EditText)findViewById(R.id.senha);
        manterConectado = (CheckBox)findViewById(R.id.manterConectado);

//        TextView link = (TextView) findViewById(R.id.gitlablink);
//        link.setMovementMethod(LinkMovementMethod.getInstance());

        SharedPreferences preferencias = getPreferences(MODE_PRIVATE);
        boolean conectado = preferencias.getBoolean(MANTER_CONECTADO,false);

        if(conectado) {
            startActivity(new Intent(this, DashboardActivity.class));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.principal_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void entrarOnClick(View v) {
        final String usuarioInformado = usuario.getText().toString();
        final String senhaInformada = senha.getText().toString();
        if("x".equals(usuarioInformado) && "x".equals(senhaInformada)) {

            SharedPreferences preferencias = getPreferences(MODE_PRIVATE);
            SharedPreferences.Editor editor = preferencias.edit();
            editor.putBoolean(MANTER_CONECTADO,manterConectado.isChecked());
            //editor.commit();
            editor.apply();

            startActivity(new Intent(this,DashboardActivity.class));
        } else {
            final String menssagemErro = getString(R.string.erro_autenticacao);
            Toast toast = Toast.makeText(this,menssagemErro,Toast.LENGTH_SHORT);
            toast.show();
        }
    }

}
