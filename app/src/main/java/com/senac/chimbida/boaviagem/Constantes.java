package com.senac.chimbida.boaviagem;

/**
 * Created by chimbida on 4/24/2017.
 */

public class Constantes {

    public static final String VIAGEM_ID = "ID";
    public static final int VIAGEM_LAZER = 1;
    public static final int VIAGEM_NEGOCIOS = 2;

    public static final String HOSPEDAGEM = "HOSPEDAGEM";
    public static final String TRANSPORTE = "TRANSPORTE";
    public static final String ALIMENTACAO = "ALIMENTACAO";
    public static final String OUTROS = "OUTROS";

}
